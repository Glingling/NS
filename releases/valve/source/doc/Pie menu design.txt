Even numbers of options best
Non changing UI best (options always are in the same place)
Good to always show you which options are available or unavailable 
Pick an intuitive direction that fits the option chosen (overwatch could be straight up, indicating your firing direction; forward)
Intuitive 'undo' or toggle action at the same location on the menu (probably the only time you 'change' options)
Pie-menu doesn't need to pop up always, allow option to delay popping it up for advanced users
Grey out unavailable, show text indicating why
Make most common options move from the center to the right and/or down.  Other common options along the primary NSWE axes.  Less common options along diagonals and left/up
8 options max, more than that becomes difficult to remember and execute
Leave a couple options open for expansion.  Try to NEVER move options after shipping.  If you have to provide option for old and new ways.

How does this work for the aliens?

Layout-
	Gestures- These can really take advantage of the layout.  Put on right hand side as they are hand gestures performed with the right hand!
		Advance forward (north)
		Fall back (backwards)
		Hold up (left or right)

	Communication (put south as used often and time-critical)
		Team chat (right, more often used)
		Public chat (left, not needed in the heat of battle as much).  Make sure not to put public and team chat on same axis to avoid moving mouse too far and telling the world your team's secrets

	Upgrades- Not used often so sideways or non-main direction appropriate?


	Equipment/weapon - South indicating self?  Or North for overwatch?  Try North first.
		Automatic overwatch/disable automatic overwatch (forward is most intuitive, gun direction).  
			Icons change here, as does option.  Show state on UI as Overwatch label.
			Only problem, this doesn't fit into a category easily.  Make label clickable?




First pass:

Communication on one axis
Weapons/equipment on the other
South is personal/team
North is public/enemy




                                            Overwatch  
                                          \  toggle   /
                                           \   |    /
                                            \  |  /
                        Public           --- Weapons -----            
                         chat              /   |  \                  Proceed
                     \    |     /         /    |    \              \    |      /
                      \   |    /         /     |      \             \   |    /   
                       \  |  /                 |                     \  |  /     
      "Fall back!" --- Communication-------- center ----------------- Gestures ------ Halt
                      /   |  \                 |                    /   |  \     
                     /    |    \           \   |   /               /    |    \   
                    /     |      \          \  |  /               /     |      \ 
              "Cover me"  |   "Roger"    -- Upgrades ---                |     
                        Team               /   |  \                    Fall
                        chat              /    |    \                  back
                                         /     |      \ 
                                               |        


Option 2:
Put default action as center of menu?! Just make sure this action is
representative of the options around it.  Unconventional but it could work...
Shows text with each button
West side is voice comm
North is weapons and equpment
South is upgrades and "econ"
East side is a bit awkwardly catagorized as "misc. communication"
Game design gets lots of control over what people say and do and how often by placement!
Add option to flip everything over vertical axis for left-handed players?
Being physically limited to options forces only the most relevent options to be included.  Good
for implementation time and simplicity.


                                    Ready
                                    Rocket        Arm grenade (1 left)
                                          \    |      /
                                           \   |    /
                                            \  |  /
                                 Welder (NA) Toggle
            "Cover me"                  -- Overwatch --- Bind 
                    \                      /   |  \      wounds       Proceed      
                     \    |      /        /    |    \              \    |      /
                      \   |    /         /     |      \             \   |    /   
                       \  |  /       Reload    |                     \  |  /     
             --- "Alien spotted" ----------  center ---------------- Team chat ------ Halt (mid between fall back and proceed)
                      /   |  \                 |                    /   |  \     
                     /    |    \           \   |   /               /    |    \   
                    /     |      \          \  |  /               /     |      \ 
              "Got ya     |    "Roger"   --  Default --- Armor          |     Public
              covered""Fall back!"           Upgrade     upgrade        |      chat (off axis from team chat)
                                           /   |  \                    Fall
                                          /    |    \                  back
                                         /     |      \ 
                                               |     Armor piercing   
                                                        rounds
                                                         (10 more creds)








