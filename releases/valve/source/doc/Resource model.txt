Social model #1 (old model)
---------------------------
a) Commanders get points depending on the amount of subordinates that are alive, and the general health of his subordinates

b) Leaders get points in the same way as the commanders, but also on their proximity to their subordinates

c) Soldiers get points for killing aliens, but they are split with the rest of the team


Social model #2 (new model)
---------------------------

Rules
-----
a) Chain of command is largely randomized at beginning of match (see exceptions below)

b) Subordinates don't have their own resource count

c) Whenever anyone is squad kills an alien, points are given to the squad leader

d) A squad leader is the only one that can buy weapons or equipment for his subordinates.  He can buy stuff for himself as well.

e) When a marine or leader dies, he respawns automatically if the leader has enough points (they are automatically spent).  He, of course, loses all his weapons and equipment.  He drops his current weapon (or everything BUT his current weapon? it was destroyed...prevents people from letting teammates die so they can take their toys)

f) Leaders can only use one weapon: the basic machine gun.  They can deploy turrets and mines however.

g) Soldiers can use all weapons.  They can use repair guns and grenades.

Side note: squad leaders get laser designators they can use for communication.  The commander gets one as well, but it's a different color.

Gameplay this encourages
------------------------
A soldier will need to show his superior that he deserves rewards.  A good soldier that doesn't get rewards won't be a good soldier for long.  A bad soldier who is stuck with a wimpy weapon and a repair gun will learn to use the repair gun to help his buddies or he won't get a bigger weapon.  TKers and people swearing constantly or holding auctions won't be rewarded.  True team players that actually make themselves useful will be more likely to get benefits in the future.  

Leaders won't have any reason to hold back goodies from soldiers; if soldiers are well equipped, they won't die as often and the leader won't have to pay to bring them back (it's automatically spent).  This encourages the leader to spend where it's most valuable, but can still have preferences that will cause others to act in line.

Soldiers are true soldiers, concentrating on using what they have at their disposal, not making strategic decisions or learning UI or relative merits of flamethrowers vs. flyers.  This is a great learning curve for people coming in from DM.  They are given weapons and equipment, it's all operated the same way.

Leaders are making the strategic decisions, letting the soldiers do the majority of the fighting.  They can spend big amounts of points, but it will never be for the leader's personal benefit (because he can't beef himself up much).

Because the chain of command is randomized every round, a selfish leader will be treated as such when he's serving the ranks.

Soldiers and their leaders will tend to stay in a group because a) leaders watch soldiers to see who deserves more merit b) soldiers need to be touched by leader to get new weaponry and equipment and c) the leader wants to stay alive and can't use the big weapons himself

Leaves room for some neat psychological stuff where marines can try to look like they're taking more risks than they are (this happens in real war)


The Big question: What does commander do?
- This is an added feature, it isn't integral to the rest of the resource model.  It can be added or omitted without affecting the rest:

...He sits back at base, protected in some kind of computer-coffin thingie.  He has a kind of spectator mode that he can use to view all subords.  He can use switch between the views of his leaders (only) or can freely fly around a hunter-seeker camera device to see deep into alien territory.  From this device he can use his laser pointer.  This device can be killed and it will respawn automatically back at the ship after a time.  His camera device emits a light as well so he won't be used for sneaky recon.  Potential other feature: the commander could be the one choosing technologies for the rest of the team.  He would have to research certain techs before they become available for purchase by the leaders.  If he chooses nothing, a default choice will start to be researched.  The research rate loosely depends on the amount of resources the entire has earned, divided by the time passed (it could be increased maybe 50% but can't fall below a reasonable value).  The commander would choose things like heavy machine gun, heavy armor, phase gates, computer-aided accuracy (for overwatch mostly), flamethrower, jetpacks, turrets, siege turrets.  The research icons showing each tech would be shown on the HUD of all players, including the progress, ie, time until finished.  Some upgrades (like defense lasers) would be given automatically to everyone when complete (or maybe after a soldier's leader equips them by touching them).

Commander controls all "party" upgrades!  I've always wanted an online party, maybe this is the way to do it.  All his upgrades are universal, game-changing and immediate.  He is also the force of drama for spectators.
He could do:
	- Give all marines get jetpacks
	- Give all marines built-in motion tracking
	- Dim the lights?  Bad idea.
	- Teleport everyone back to base?
	- Cause a player to get "blaze of glory" (he goes berserk and can't die for a time but then WILL die)
	- Give a rallying call to give everyone an adrenaline boost?  Temporarily increase current speed and ROF of everyone in radius!
	- Place a phase gate to provide an escape (or a place to attack!)
	- Mark a enemy target or a friend that needs help
	- Def matrix a marine

Usable abilities
	Marine - Can buy: nothing.  Can use: All weapons, mines/claymores, jetpacks (automatic), repair gun, 
	Squad leader - Can buy: all weapons and equipment.  Can use: Basic machine gun only, ammo dispenser, turrets, has laser pointer.
	Commander - No weapons, invisible laser pointer, party/drama upgrades, create camera outpost, siege turret (below).  His role is to manage the future of the team and he is ultimately responsible for everyone's lives.  He pays for all respawns.

Random thoughts:
  Instead of "resource buildings", how about a big cool-looking structure with cameras, lenses and other listening/broadcasting devices on it.  If an alien is killed where one of these could "see" the death, the marines get more points for it.  It shows the actuarial computer that the marines are kicking ass and is war-time propaganda.  Maybe it works the same for aliens, the marines lose more points when dying near such a thing?  This structure could be used by the commander (somehow) as well.
  Keep a history of a soldier's use with a weapon.  When a squad leader looks at a soldier, his profile reads out, so the squad leader can see what they're good at and what they've never been able to try.  Also means more consequences for a marine's action. (include # of TKs with weapon?!)
  Make healing/repair gun work very quickly so it's worth a player using it instead of fighting (or instead of another target).  It should be used to help a player deploy equipment or use a weapon when it's really needed.  It could be a repair gun and have armor/health work like the Protoss (it is more useful to use sooner rather than later)
  One goal should be: have one "buddy thing" happen every 5 minutes of play.  Being healed, healing, being given something, giving something, saving someone, being given a sensible order, advancing together with communication, fleeing a bad situation together, etc.  This is what will keep people coming back to the game.  It should feel like "Remember that time when YYY and I were holed up in the med lab waiting for the rest of our team while the aliens were trying to get through the door...I was so scared until the team got there.  Boy did those aliens get a surprise"
  Lots of meaningful and stuff can happen with a simple resource model and no commander or chain of command, don't force it in
  When given a weapon by squad leader, switch to it automatically
  Pie menu: 
	Soldier: "light", "heavy" and "other" slots (down, up and right)

        			  Reload
		         	    |
           			  Weapon - Drop    Repair
		                |	     	     |	     
      - START       -  Use ----- 	 Equipment - Mines
		  |	            |	             |
Gesture -Comm      Light/Heavy Smoke - Grenades - Nerve gas  (smoke grens don't affect turrets or overwatch!)
		  |								 |
Public - Chat - Team					Frag
		  |
		Nearby
		  

	

	Squad leader: Can only buy one item at a time.  After deploying it he can buy another (next marine he touches receives it).  

          grenade launcher
             	  |
        nuke -	heavy - flamethrower
				  |
       Repair	  |		            Drop item
          |		  |         |		   |
HvyArmr- Other  - Buy  -  START  -  Current
          |		  |			           |
        Turret    |              Toggle MG/laser ptr
				  |
        MGupg - light - shotgun
         		  |
              mines/clay

	
	Commander: (swap "help" with "orders" to be consistent...most commonly used on right, purchasing done to left)

                      Motion tracking
                            |
            LifeSupport - Team - Jetpacks
                            |	
MoveTo-Area	                |	Camera tower
       	|				    |		 |
        Orders - Start	- Help -  Build - Phase gate
          |					|		 |
        Target - Repair     |      Siege
          |    Adrenaline-Marine - Blaze of glory
                            |
                         DefMatrix

Add more tech nodes that "upgrade", ie, a normal tech tree.  This makes the decisions much harder to make otherwise any option can be chosen at any time.
 - Maybe you have to buy "team", "build" or "marine" first?
  
Potential problems
------------------
a) Newbies.  Your mod goes out there, no one has a clue what is going on...
some people get randomly assigned leadership roles.  They get confused...
this mod sucks!  Never play it again.  Learning curve too steep.
- Check how many times a player has played on a server.  The first x times, he will always try to be a soldier.  Does this work?  What about duplicate nicknames or server-hopping?  It doesn't have to be a perfect system, but it should work the majority of the time.  What happens when a whole team is filled with newbies...ie, on a LAN!?!
- Address through training.  Try to encourage playin on the training servers...is this a reality or theoretical happy land?
- Make the commander an optional role.  If the game "decides" a player can be the commander, a message is broadcast telling him so.  He now has the option of walking over to the commander area and jacking in.  He can hit enter to leave the commander's chair and come back into the game but if he does, how does he fit into the COC?  Maybe he is only allowed to join during the first x seconds of the game?  Doesn't address the differences of squad leaders.

b) The randomness of c.o.c.  It solves some problems, but seems to create a
slight problem too.  What if someone REALLY wants to play a commander?  I.e.
hes not the best dm'er but would love to order guys and do a good job about
it.  How does he get to play?  Waiting 14 rounds for his turn?
- 7 rounds on average, but that's still totally unacceptable, esp. given the longer average time of each game
- Kind of the same as a).
- Does it make sense that you could have a good commander that hasn't fought as a soldier?  Ie, how can he understand the strategic weapon/tech choices vs. different types of aliens if he hasn't seen them first hand?
- The game could remember the % times you individually won or lost when you were in a certain role and tend to put you in the role more often?
- Allow total and utter choice over role you spawn in as.  If your team is all soldiers or commanders, so be it.  If the choosing of roles is somewhat iterative and reactive (ie, choosing during pre-game by standing somewhere) then people who don't mind different roles would tend to choose roles to balance out the team.  This would inevitably lead to games where the team is weaker than it should be though.  Or an upper limit on superiors could be established by limiting the space that was occupied, but the team could be all soldiers if they wanted to.  Careful, by trying to be all things to everyone, it will be nothing.

c) The research thing.  Will being a commander appeal to the dm online crowd?  Maybe not so much.  Or to a few people, but these people might not like it for reason B.
- Researching sounds like a dud if you see what Science and Industry did with it
- Get rid of commanders altogether.  It would be nice to have a chain of command, rather than squad leaders and soldiers, but only if he can make meaningful higher-level choices.  It's also hard when there are no resources he has control over.
- It would be nice to have commander's that could keep the squad leaders in check, someone they somewhat controlled by
- Make commanders more like squad leaders.  There are certain things they can buy and deploy that no one else can, ie phase gates, def lasers, and camera posts.  The squad leaders could use some heavy weapons but not all that a soldier could.  Everyone carries a weapon, though the commander wouldn't be on the front lines often.  This would open the game up to more people than DM, but it wouldn't accomodate the non-military commanders, meaning this problem goes away a bit.

d) People won't "buy" the whole concept of subservience and won't like the idea that they can't always make their own weapon/equipment/role choices.  DMers might not like the marines at all because of this.

e) Instead of TKing, a player that is a leader could just not buy upgrades for people.  Wouldn't they have more fun as an alien player?  What if they tried to join aliens but the team was full?

f) Mega-big showstopper: what to do if everyone wants to play as a marine?
- Display message: "Your team <still> needs a squad leader.  Use your rank menu before the match starts to volunteer."
- Start "Sparring/Deathmatch mode".  Marines get weapon upgrades and ammo periodically, as needed.  Most upgrades aren't enabled, suitable for small games that focus on sparring and combat.  A pleasant sub-game that should always be visceral and fun.  
- After number of DM games played > n: "You would make a good leader, how about giving it a shot?  Use your rank menu now to volunteer."




System #3
-----
a) Everyone is a soldier until someone promotes up to a leader or commander

b) Squad leaders and the commander have their own resources, marines only fight

c) Whenever anyone's squad kills an alien, points are given to his squad leader.

d) A squad leader is the only one that can buy weapons or equipment for his subordinates.  He can buy stuff for himself as well.

e) When a marine dies, the leader chooses whether to bring him in as a reinforcement or not.

f) When a leader dies, mariines are notified and they must touch his corpse to bring him back.  If he isn't brought back, his points are split between the other leaders and commander.  The marines change to a new squad.

g) Leaders can only use one weapon: the basic machine gun.  They can deploy turrets and mines however.

h) Soldiers can use all weapons.  They can use repair guns and grenades.

i) Marines get resource injections, the amount of which depends on their current points.

j) The aliens get resource injections, the amount of which depends on how many hives they have.

k) The aliens start with a randomly placed hive.  Every n amount of time, they get another hive at another random location.

l) Whenever an alien kills a marine, he gets points.

m) Commander flies around and has a laser designator.  If he "dies" he respawns back at the ship and must travel to get to a destination again.

n) The commander spends resources out of the pool from all the squad leaders.  He sees how many resources each has and the cost is split between all of them

o) When a squad leader dies, he respawns back at the ship...his subordinates respawn back at the ship with him, and it costs enough everyone to respawn.



System #4
----------
a) Everyone is a soldier until someone promotes up to a leader or commander
- Code done

b) Squad leaders and the commander have their own resources, marines only fight
- Code done

c) Whenever anyone's squad kills an alien, points are given to his squad leader (or split with his leader and commander if he has a commander).  Given to the soldier if no leader.
- Code done

d) A squad leader is the only one that can buy weapons or equipment for his subordinates.  He can buy stuff for himself as well.
- Code done

e) When a marine dies, the leader chooses whether to bring him in as a reinforcement or not.
- Changed so he always comes in
- Code done

g) Leaders can only use one weapon: the basic machine gun.  They can deploy turrets and mines however.

h) Soldiers can use all weapons.  They can use repair guns and grenades.

j) The aliens get resource injections, the amount of which depends on how many hives they have.

k) The aliens start with a randomly placed hive.  Every n amount of time, they get another hive at another random location.

l) Whenever an alien kills a marine, he gets points.

m) - Commander has no body and can only view combat from cameras mounted on shoulder of each squad leader.  
   - Pressing 1-3 first selects the squad leader, pressing it again will go to it.
   - Commander can free look when in a camera, it swivels the camera when he moves
   - Orders can be given to the currently selected squad leader: hold this position, advance 10 meters, fallback, move to location, support squad, warning.
   - The context is automatically implied from the camera that the commander is viewing through.
   - Only squad leaders hear the orders, though perhaps the marines hear some indication that an order was given to the squad.
   - Squad leaders give orders to their squad like, "around me", advance, fallback, hold here, incoming
   - Very important that players know the commander knows more about the current game environment than they do, so they'll be tense and follow orders

n) When a marine dies, he respawns at the expense of his leader. (possibly only expense is implied cost of weapons?  this way mean marines can't drain their leader's resources)
- Code done

o) When a leader dies, he respawns at the expense of his commander.
-- or how about, a leader pays for it himself and can only respawn if all his marines are alive?  Once he's out, his surviving marines will defect if possible and still be able to play
- Code done

p) If a marine wants to defect from his squad, he immediately gets his share of the resources awarded in 
  ammo and the best weapon he can, then is assigned to the next squad he hasn't been part of this match.  
  - Code done
  
  p-a) If no squad exists, he has no squad and is on his own.  
  p-b) If he dies, he can't come back.

q) If a marine defects and a leader no longer has any subordinates, he is on his own and cannot respawn.  His resources are immediately forfeited to the commander.

r) When all squad leaders are dead or orphaned, the commander can only inhabit turrets and home base.
Need to be able to
- give a marine's leader points
  - Code done
- remember the leaders a player has defected from
  - Code done
- check if a leader has any subordinates, and which ones died
  - Code done
- be able to find the leader of any squad (for commander possession)
  - Code done
- tell if a player is in any squad
  - Code done

