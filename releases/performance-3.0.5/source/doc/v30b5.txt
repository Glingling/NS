Features and bug fixes for beta 5
---------------------------------
Charlie
- Evaluate new map: thor
O Update ns_metal (in "to backup", dated 5/17)
- New Guide icon for newly-relaunched Guide program (if program ready in time).
- Remove co_core because no source?
	- Posted note for Merkaba
X Other CPL-specific stuff
- Evaluate BrigadierWolf's submissions
O Dev/team/forum changes/updates
O Evaluate altair
O Removed Xfire from installer
O Updated Steam browser description and picture
- Adjustments for large #s of players
	- Try PTing scaling of resources
	- Try PTing scaling of respawn times
	
O Updated ns_metal
	* More infestation/Tweaks
	* Optimized surface access
	* Fixed 3 build exploits
	* Removed weldable pipe(shortcut)

O Test co_basic (not pretty enough)

O Added two missing sounds in ns_agora

O Updated ns_bast
	- Reduced size of the vent on top of feedwater so marines have to crouch
	- Fixed button on the Engine Door
	- Fixed a lot of areas where the comm shouldn't build (this includes all the weird feedwater spots)
	- Raised Feedwater hive to avoid exploits
	- Moved the Feedwater resource a bit so you can get behind it
	- Moved engine and added a second exit to the hive
	- Moved the res outside of Refinery to Water treatment
	- Made the revolving door unusable (like the one on the other side)
	- Lowered Marine Spawn closer to the cargo level and removed the elevator
	- Added new area between the Marine Spawn and the Engine corridor
	- Moved the new res on the bottom of the main aft lift closer to Tram Tunnel.
	- Reduced excessive brightness in some areas (lights over the nodes, feedwater)
	- Added vent between Feedwater and Tram Tunnel
	- Tweaked the Tram so the Onos can fit without crouching
	- Fixed strange clipping holes and errors (this includes some stuck issues too)
	- Added lights to the underwater tunnel

O Updated co_ulysses
	- Fixed bug in Hive ramp
	- Changed and added detail at Cargo Bay, Marine Spawn and Hive.
	- Added a new window to space at Cargo Hive.
	- Modified some lights along the map to make it less “discotheque” looking.

O Updated co_daimos
	- fixed stuck issue in Lower Sewer (bug #0000590)
	- grates can now be shot through (bug #0000692)
	- added a vent from Hive Area to Upper Tram to allow Alien to fall into the Marines' back there.
	- added a vent from Hive Area to Exactly above the PG welding point.

O Alien resource nodes don't play gurgling sounds until a minute into the game (prevents commander from listening for starting hive)
O Lowered turret factory build time from 13 seconds to 9 seconds
O Movement chamber range increased from 400 to 500
O Movement chamber energy bonus increased from 10% to 25%
O Changed healing spray effectiveness vs. players: changed from 16 to 13 + 4% of target (both for healing friendly players and attacking enemy players)
O Increased Gorge armor from 40 to 50
O Increased Onos carapace armor bonus from 150 to 350 (makes it closer 50% like all the other aliens)
O Changed hand grenade physics to make them more effective at attacking indirectly
O Increased redemption chance from 35% per second to 45% per second (still happens when at or below 40% health)
O Increased spit damage from 25 to 30
O Grenade launcher cost lowered from 20 to 15

Combat changes (thanks Forlorn!)
	O Aliens always respawn as skulk, even if they were previously a higher lifeform
	O Dying with a larger lifeform gives you back points spent on it
	O Unchained alien lifeforms, like regular NS:
		O Gorge costs 1 level
		O Lerk costs 2 levels
		O Fade costs 3 levels
		O Onos costs 4 levels
	O Lifeform evolution times increased to 75% of those in regular NS
	
O Changed version to "beta 5"

Not checked in (previously mentioned)
---
X Electrical nodes take energy instead of health (adren counter-acts electricity exactly)
	X Cost lowered from 30 to 20
	X Research time lowered from 30 to 20
	X 33% alien energy is reduced on each hit.  Structures are not affected affected by electricity.
	X Fix energy issues
X Distress beacon siren now emanates from the marine spawn area instead of the observatory (average of marine spawn points)
--

O Updated ns_hera
	O Added missing titles
	O Added a resource node in north-west corner of the map, by the weld to the rock cavity.
	O Removed a node from Holoroom
	O Removed ground path from Cargo to Holoroom
	O Added in a balcony overlooking the Holoroom, accessible from the upper level of Cargo.
	O Moved the node in Cargo back to it's original place - down in the pit beneath the door.
	O Re-routed Maintenance Access' Cargo exit to beside the cargo life next to MS. (Just so the exit doesn't come out at the resource node)
	O Enlarged the general area around the CC, as well as fixed the getting stuck problem around it.
	O Fixed being able to see into the vent hive as commander with no marines around.
	O Added a vent between Archiving hive and North Corridor (heralocation_ncorridor).
	O Added a vent from broken security door to cargo lift.
	O General fixes and modifications.

O Updated ns_bast
	O Fixed two new clipping errors
	O Fixed excessive brightness in the marine start

X Reduced jetpack cost from 15 to 10 (Jetpack cost back to 15)
O Increased jetpack maneuverability, jetpackers are now immune to stomp and carried items/ammo affect lift less
O Decreased Combat gestation time
O Changed mp_limitteams default from 2 to 1 (in server.cfg and listenserver.cfg)
O Undid turret factory build time change (back to 13) until turret farming hives can be resolved properly
O Decreased grenade bounciness and increased gravity (I think this is perfect for everyone now)
O Fixed bug where the hive take-damage animation interrupted itself
O Fixed bug where HPB_bot wasn't working (internal use only)
? Combat spawning changes (reprise)
	O Players spawn in waves, with a maximum wave size of 5 players
	O A wave starts when there is no wave in progress and a player dies
	O The wave finishes when 5 seconds + (number of dead players - 1)*4.5 seconds have elapsed
	
- Increased welder priority so grenades appear below welder (so they aren't chosen by accident)
- Tweak alien res model for large/small games


Changes from XP-Cagey (the details are a bit fuzzy still, as I'm doing his check-in for him)
O 'lastinv' is now client-side predicted
O fixed bug where aliens sometimes spawned without a weapon
O Fixed occasional 'garbage' in scoreboard title
O Tweaked weapon 'weights' so grenades and other droppables are prioritzed better
O Fixed bug with heavy armors in the minimap
O When players in the air and are knocked-back, don't send them flying away so fast
O Fixed bugs recently introduced in structure placement (especially resource towers)

-O Changed method of giving players weapons/heavy armor/jetpack in combat mode, players should never seen a weapon or armor module on the ground ever again.
-O Players can no longer delay their death by issuing kill command again.
-O The sound when an alien building is placed only plays if the building is successfully placed.
-O Fixed bug where evolving from a higher lifeform to a lower lifeform with low health would give you 0 health, but you would still be "alive".
-O Fixed missing STEAMID's from some log messages.
-O Changed method of locking cvars to one thats faster and cleaner.
-O Added missing newline character in log message.
-O Parasite flag now removed when a mapper placed building is destroyed.
-O Aliens no longer take negative damage when they have 4 or more hives.
-O Chat box code now sends strings as a single argument rather than multiple arguments.
-O Fixed bug with health message being spammed.
-O Commander can now click on switches to activate a door or elevator.
-O Aliens can no longer evolve in mid air.
-O Parasite flag is now removed from players when they die.
-O Aliens will no longer show up on motion tracking if they are dead.
-O Fixed "Game reset started" and "Game reset complete" log messages from sometimes having wierd characters.
-O Added cvar "s_show" to locked cvars. (Forced to 0)
-O Aliens now that get healed while evolving will keep thier health percentage once they finish evolving.
-O Removed old flashlight battery code to prevent bug where players could run out of flashlight energy.
-O "slowresearch" cheat now slows down recycling of buildings.
-O Improved knockback code, HA takes less knockback than LA/JP'ers, less knockback is also applied if they are in the air
-O Changed knockback of bitegun from 300 to 225. 
-O Knockback _never_ affects friendlies.
-O Parasite and xenocide now get damage boosts by primal scream.
-O Commanders can no longer use a chair while its recycling.
-O +attack and +jump can no longer be bound to mousewheel with mp_blockscripts 1.
-O Added lightgamma to locked cvars. Value cannot be set below 2.0
-O Removed ex_interp from cvar locks (I dont think its exploitable anymore)
-O Added locking of r_detailedtextures to 0 due to exploit.
-O Commanders can no longer use thier use key, they dont need it anyways (to prevent phantom use sounds).
-O Weapon upgrades now affect turrets.
-O Aliens can now evolve on slopes without having to jump.
-O Aliens can now evolve again in the air (this was a previous change by me, which was the incorrect fix for the above)
-O Fixed odd problem with alien regen, aliens could sometimes get insane health.
-O lastinv no longer plays sound when used.
-O Tweaked welder sounds a bit, welder now plays different sounds when its welding something & when its not welding, so its now possible to tell if your welding something or not.
-O Fixed alerts not playing when a 'friendly' gorge attacks a building with FF on.
-O Silence now affects the 'response' scream from players if they have silence.
-O Silence now silences the scream emitted before a xenocide.
-O Limited the updating of balance vars to 10 per second to prevent overflows.
-O Removed automatic switch on alien weapons (added by cagey when lastinv was put in) due to odd crashes.
-O Tweaked welder some more, added 2 balancevars to tweak how welder acts on players vs buildings.
-O Lowered chat text bit (it was way too high).
-O Changed channel of welder sound (to prevent it from cutting out when someone says something)
-O Made a slight tweak to evolving, players should always be able to evolve if theres enough room.
-O Digesting sounds are now quieter/silent if a player has silence upgrade.
-O Blasts from onos gore, xenocide, no longer affect teammates, ever.
-O Players gestate sound is now silenced if they are upgrading to silence.
-O Waypoints shouldnt follow cloaked aliens. 
-O Spectators should be able to see cloaked alien buildings when spectating in first person.
-O Added precaches for certain sprites for mp_consistency.
-O Re-added old sound that played when points were spent. (It rocks)
-O Updated ns_nancy from Lazer
-O Updated turret sounds from madmax.
-O Made small code change with the welder (behaves same)
-O Entity build limit circles ignores non-solid entities.
-O Players that have joined a team before no longer qualify to be spawned back in instantly (prevents exploit with rushes)
-O Damage is always rounded up to the highest full number.
-O Fixed handgrenade from playing the same sound twice, now players the pin pull sound & throw sound when thrown rather than the pin pull sound twice.
-O Fixed issue with mapper placed offense chambers with the startbuilt flag on from ever working, they will now auctually be fully built & should be operational.
-O New grenade model from BrigadierWolf.
-O Updated grenade sounds from MadMaxx


Update from Elven Thief - (7/7/04)
- Fixed bug where turrets would not target or fire on underwater aliens.
- Fixed at same time issue where aliens that spawned or became submerged in water without
  moving could not be shot.

  WE HAVE A PROBLEM with playtest builds as it seems there is something with Balance.txt
  causing overflow to client builds if the server is trying to send a copy of the vars.

  ** SEE e-mail I sent to both of you (Flay)

O Reduced time it takes to through hand-grenades (thanks BrigadierWolf!)
O Changed grenade physics to a light toss when tossing grenade from crouched position
O Improved animation playback for grenade view model, including synching up grenade creation with throw

O Updated ns_eclipse
	O Removed doors in Marine Spawn
	O Layout adjustments to Marine Spawn
	O Fixed clipnode issues in South Loop (invisible "ledges")
	O Additional lighting tweaks
	O Removed added pipes in Triad - nice idea, but very hard to implement logically. 
	O Fixed damage issues on weldable doors 
	O Attempted to smooth over infestation 'terrain' in hives (single convex clip brushes) 
	O Fixed lips in vent to marine spawn and other vents for smoother navigation 
	O Replaced old infestation texture with new hotness

Charlie TODO:
- Eclipse update from KFS

Expose bunny-hopping to everyone? (+3jump automatically, or Quake-style queued jump?)
1 for skulk, 3 for fade/onos?
