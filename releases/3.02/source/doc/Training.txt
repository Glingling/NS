Features
--------
- Simple to add or reorder sections
- Fully internationalizable
- Allow skipping and replaying sections
- Form solid base for scripting engine: only general changes to NS, no hard-coding tutorial behavior
- Sections of tutorial are designed to be run independently.  Don't assume previous knowledge from one tutorial, nor assume any order.

Overall code structure
----------------------
  I think we should have some general finite state machine that will call sections, and keep calling them until they finish.  Each section could be a function named
using this the tut_xy convention and they could each specify if they have been completed or not.  Once this is established, we should be able to hook in new sections
easily, with minimum work.  It should also easily allow bypassing of sections using the "need order" command.  Each section would need to be called such that it 
can be forced to execute it's ending conditions.  I'll try to come up with this structure.  Once this is in, I can add the last hooks needed for the script behavior, 
and the rest should be cake.

function TutM1Init()
end

function TutM1Run(int inRunMode, string inText)
	' Run mode 0 == reset
	' Run mode 1 == execute step
	' Run mode 2 == give hint or repeat instructions
	' Run mode 3 == skip past step

	' Return code 0 == Step didn't complete
	' Return code 1 == Success, step completed
end

function TutM1IsComplete()
	' Execute code when run is completed or skipped
end

function TutM1Completed(bool inShowAudioVisualCompletion)
	' Execute code when run is completed or skipped
end


' Shell code
	' Keep track of current part of training (g, m or a)
	' Remember current step
	' If time for next step?
		' Run current training, with current run mode
			' If 0 or none
				' Run current step, mode 1
					' If return value was 1, increment training for next tick
					' Else if return value was 0, next step will be hint, set next time
			' If 
		' If current step 
	' Else check if current step has been completed
		' Call isComplete
			' If so, increment 


Tutorial
--------
All tasks are labelled as one of the following:
	- map (meaning it's a map effect)
	- script (meaning it's done via the scripting system)
	- code (meaning the capability isn't there yet, and it must be added) 

All the text that's displayed will be stored in titles.txt.  Name each bit of text with a nice naming convention, along the lines of the following:
Tutxy - Where x is R for ready room, M for marines or A for aliens and y is a number starting with 1.

This will let us easily tweak and internationalize the text without changing any of the scripts.

Example:
	script: "TutM3" -> titles.txt: "You are a soldier, and you are equipped with a machine gun, pistol and combat knife.  Press your left mouse button to try firing your machine gun."


Go up lift
	- Show intro credits (map)
	- Show lots of cool stuff to get you in the mood, and get your senses primed

Land in ready room
	- Suppress countdown and regular MP player help (script) (code)
	- Suppress initial resource tower completion (or remove it then add it when necessary?)
	- Enable cheats (allows evolving to all aliens in tutorial with only one hive, makes building faster)
	- Explain the concept of a ready room (script)
	- Explain how to move (WASD, or arrow keys) (script)
	- Tell the player to choose one side or the other (script)

Soldier training
	- Suppress ability to use command station (code, script)
	- Welcome to marine training. Take this lift down to the training area.  (script)
	- Give little flavor speech on the way down (being a TSA marine means sticking together, acting as a team, and following orders.  If you don't want to do this, perhaps you better find another unit.) (script)
	- Prevent player from advancing past hallway at bottom of lift (map)
		- If at any time the player asks for orders, skip past this phase of training
		- "You are a soldier, and you are equipped with a machine gun, pistol and combat knife.  Press your left mouse button to try your machine gun." (script)
		- "You can switch to your other weapons with the mousewheel, or by pressing the number keys, 1 through 3."
		- Explain pop-up menu
			- "Hold your right-mouse button to access your pop-up menu.  You can move your mouse over an area to see more options.  Move the mouse left to "orders", the select "need order".  (if not detected in 5-10 seconds, repeat, script)
			- "Good.  Try a taunt by using your pop-up menu.  Hold your right-mouse button, select "Comms", move to "talk" then to "taunt" (wait, repeat if too slow, script)
		- Open gate at end of hallway via a map trigger (script fires trigger, map links trigger to gate)
		- "Good.  Now move to X to learn about military structures" (script)

	- Military structure training
		- When player reaches info_location... (script) (code)
		- "Commencing, military structure training.  If you want to skip this phase of training, use your pop-up menu to ask for orders" (script)
		- Point out resource tower. "This is a marine resource tower.  It collects resource for your team, and allows the Commander to research new technologies, and equip the team."  (script finds location of nearest resource tower and sends tooltip to point it out)
		- "All structures must be built before they are activated.  A soldier can build a structure by pointing at the base of the structure and holding his 'use' key.  Try it." 
		- Check build state of this entity.  If it doesn't increment at all within a few seconds, repeat the order.  Repeat until structure is built. (check build state of resource tower, proceed when at 100%, script)
		- "Good.  Now the resource tower is fully built and will collect resources for your team."

	- HUD training
		- "HUD training commencing.  To skip this phase of training, use your pop-up menu to ask for orders" (script)
		- "Your team's resources are always displayed at the top of the screen <point out>.  Notice that every few seconds the resource tower adds a few more points to your team's resource total" (point out resource total, script)
		- "Only the team commander can spend resources.  As a soldier, it's your responsibility to fight enemies, build structures, and protect them." (pause, script)
		- "This area of your HUD shows you the name of your commander.  Notice that you have no commander right now.  In a real military exercise, it's very important that you have a commander at all times.  A squad without a commander has very low survival odds." (pause, script)
		- "Your HUD also shows your current health" (point, script)
		- "..your armor" (point, script)
		- "...the ammo in your clip, and your reserve ammo (point, script)
		- "When your ammo is low, you can reload your weapon using the pop-up menu, or by pressing your 'r" key."
		- "Your HUD has advanced entity recognition software that will describe objects you encounter.  If you see something you don't recognize, point your reticle at it and you'll see a description in the lower-left region of your HUD." (pause, script)
		- "Your HUD also displays the structure around you, as well as teammate positions and known enemy locations.  Use this data even in battle, it can save more lives then your own." (point out, script)

	- Orders training
		- "Orders training commencing.  To skip this phase of training, use your pop-up menu to ask for orders" (script)
		- "During a military exercise, your commander will give you orders using his command interface." (pause, script)
		- "Here's a test order.  The blinking blue circle on your screen is telling you to move to that location.  Move to this waypoint now." (script)
		- When player has completed orders: "Good.  Notice that your order was completed automatically.  Commanders often give new equipment, health and ammo to soldiers that keep their cool in battle and follow orders.  Keep this in mind when deciding whether to follow orders or not." (script)
		- "The commander has more information about the environment then soldiers do, so sometimes an order may not make sense, but you should follow it anyways.  The commander can often point out enemies before you see them." (script)
		- Create infantry portal nearby, with a build order (code, script)
		- "This is what it looks like when the commander creates a new structure, and needs you to build it.  Build this structure by pointing at the base of it and holding your use key." (script)
		- (when built) "Good.  One last thing about orders: when you need something to do, use your popup menu to ask for orders (left, up).  After receiving orders that you intend to carry out, use the pop-up menu to acknowledge them.  The commander will be grateful."

	- Objectives
		- Teach players about alien menace?  Would that be weird when adding marine vs. marine later?

	- "Soldier training complete.  To begin commander training, walk to the command station and press your 'use' key."

Commander training
	- 


Alien training
	- HUD stuff
		- Energy
		- Individual resources 
		- Weapons
		- Hive sight

	- Pop-up menu
		- Evolving to new lifeforms
			- Change to builder
				- Hives
					- Kill existing hive
					- Explain what hive locations look like
					- Build new hive
				- Build turret

	- Alien structures and builder training
		- Resource towers
		- Offense chambers
		- Hurt a building, then show player how to heal it
		- Upgrade chambers

	- Upgrades
		- Blinking icons
		- Choose an upgrade, show level
		- Explain that once an upgrade is chosen, it is automatic (maybe not needed)

	- Ability to try out all lifeforms
		- Give resources often
			- Explain web
			- Explain wall-running
			- Explain flight
