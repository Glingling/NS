

  One of the biggest attributes that make games compelling is when there is a high consequence to the player's actions.  When a player's every decision has a significant consequence (or perceived consequence), the player takes the time to learn about the play environment and try to excel in it.  Significant consequences mean the potential for significant rewards.  Without significant consequences, the player realizes that what he does matters less and playing becomes a chore or task list that is merely executed.  Seeing that the player always has interesting decisions can help acheive this goal.

  Strategy games have interesting decisions built into their core.  The high-level is the choosing which nodes of the tech tree to research, when to attack and when to expand.  These all deserve significant thought on their own.  On top of this, real-time strategy games have the infinitely scalable "attention" dynamic: all your units can perform better if you micro them, and there's no theoretical limit on how much attention you can provide, if you're fast enough.  This makes RTS extremely compelling.

  Vanilla deathmatch has some interesting decisions to make, like which weapons to go for, which weapon to use currently, trying to figure out where your target is going and leading him or outhinking him, etc.  It's all at the low-level however, and there are limited opportunities where a good high-level decision is more conducive to winning then raw skill in the low-level.  For example, even if you outwit your opponent and get armor and a better weapon before your next fight, his fighting skill can easily compensate and he can still win the encounter.

  Counter-strike has this same dynamic, where it generally pays off to hone your fighting skills, more so then acting as a team, synchronizing a multi-pronged attack, typing a message to your teammate that he is about to be attacked (instead of running over there yourself or attacking someone instead), etc.  Counter-strike does add many more interesting decisions that affect the future of you and your team in big ways.  The biggest example, having to sit out after you die, makes every second of life that much more demanding.  The simple economy provides interesting high-level decisions as well.  The location-specific hit model, the necessity of reloading at the right time, the fact that you slow down when you take damage, all of these add lots more interesting decisions at the fighting level, and make Counter-strike perhaps more compelling than any other first-person game out there.  

  Even with these amplified consequences to player actions, there seems to exist room in first-person games to incorporate more high-level decisions from strategy games.  In strategy games, the outcome of high-level decisions generally beat out low-level decisions.  Choosing to get airpower, cloaked units, or choosing when and where to attack generally is more conducive to winning than the actual deploying of your forces (though the outcome still rests on that to a degree).  Generally you can just throw the right units at an area at the right time and good things happen.  However, the great thing about strategy games is that once both players are competing at a high-level, individual battles (and then games) are resolved through the execution of the low-level, the fighting, targetting and maneuvering.  If both players are equally skilled (or inept!) at the high-level or the low-level, the game outcome switches to the other.  And so it goes, the entire game alternates between strategic decisions and execution.  The thought and the fighting.  This is only possible because the great high-level strategy can be potent and competitive in the face of great fighting.  In all FPS games out there right now, high-level strategy can't genearlly compete with fighting skills.  This is a fine state of things, but it leads to an environment where only the strong survive, and only the strong play.  The question seems to be, how to incorporate potent high-level strategy into the unparalled low-level decision-making depth of first-person games?

  My first thought was to remove some of the fighting skill required, in order to compete.  By focussing on how many and what type of weapons were deployed in a battle at a time generally determines the outcome.  This means the high-leveld decisions get more credence next to the low-level ones.  Now I'm thinking this is the wrong approach, because it means the skill curve plateaus early.  After mastering the high-level, there is nothing else to get good at.  This would be like making an RTS where you can't control individual units.  Maybe it's fun for awhile, but it doesn't seem to be the grail of interesting decisions.  Maybe you could try to make the high-level decision making infinitely scaleable, like the low-level in FPS.  If you could accomplish this, the game doesn't get stale, much like the action part of FPS doesn't get stale: no matter how much you play, you can always get better.  Your accuracy can get better, you can move around the map faster, you can listen better, you can predict your opponent better, etc.  However, this too seems flawed because the number of high-level decisions made is much less than the infinite number of low-level decisions you make.  For example, the number of strategic decisions you might make in 40 minute game of Starcraft might number less than 100 (guessing).  The number of decisions you make in a 20 second firefight with a skilled opponent is uncountable, as many decisions are made as fast as you can react.  So it doesn't seem easy to make the high-level decision making of any game, let alone in an FPS, infinitely scaleable.

  So it seems that traditional high-level decisions must be added to FPS to get this depth.  The speed and scale of FPS games makes the notion of many high-level decisions absurd.  "Deploy this unit <player1> and this unit <player2> under the bridge and let me know what you see."  This might be the equivalent of leaving a couple dragoons between your opponent's base and a nearby island expansion, watching for flying buildings and troop movements.  But let's look at the problems.  Instead of selecting a couple units and clicking on a destination, you must chat to those people and try to describe to them what to do.  Even if you had an interface where you could select and indicate an order, these aren't units, they're people and they don't listen, especially to boring-sounding orders.  Then there's the problem of reporting back.  Instead of paying attention to enemy colored blips on the mini-map (that are in LOS of your scouts), those people need to report back to you when they see something.  Then there's the question of scale: most of the time, those people would be much more helpful fighting alongside the rest of your team.  Instead of having dozens or hundreds of units, you generally have less than ten.  So it seems that the environment must be slowed down, scaled down or otherwise change to let players function strategically.  The social environment is perhaps the biggest obstacle, with most people not wanting to take orders, or not understanding orders.

  So my question is (finally), what thoughts do you have on changing first-person games to allow high-level decisions to compete with low-level ones?

  I'm thinking:

- obvious material rewards for carrying out orders
- a player's equipment being purchased by another player, who is rewarding people doing good for the team
- increasing the size of maps, or otherwise increasing the amount of time it takes to reinforce an area or group, so it's not so easy to help out at a moment's notice (this isn't much of an issue in CS, if someone's in trouble they're usually dead before a friend could turn a nearby corner to help).
- otherwise promoting the idea of troop movements or deploying defense
- god I dunno anymore

  Do you have any ideas?!
  


Situations that work great in RTS that would be great in FPS
- leading an enemy into your defenses
  - silent, effective sentries

- a vigorous counter-attack after a lossy attack
  - when aliens have to spend some time to upgrade, they will be gestating or only low-level during the counter

- getting slaughtered after charging into an unknown battle

- skipping low-level upgrades and going for a big tech upgrade as fast as possible before attacking (counter should be possible too: the other team expanding/growing like mad or containment)

Goal: viably concentrating on high-level instead of low-level for a time
  - soldiers could help build something (this could be a building that takes low-level skills to build faster, like a construction point that moves around and using it or lasing it constructs faster)
  - ammo is important and costs points
  - turrets that can only be constructed by soldiers
  - ability to bunker up via sentrys, welding, mines

Goal: effective scouting
  - weight system where you can "go naked" and get big speed boost
  - radically different reations depending on enemy's actions (invisible units, melee that can't hit air, siege)
  - add buildings.  these commit your team to a strategy on a time and resource level and let's and enemy see what you're doing

Goal: "going air"
  - jetpacks for marines, most aliens have only melee

Goal: Surgical strikes
  - if buildings were needed for tech, they could be destroyed to have your tech disabled

