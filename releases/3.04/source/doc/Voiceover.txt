Lani: 619 583-2792

"You're listening to NS radio"
"Hive construction complete"


Budget and constraints
----------------------
$600 for as many of these as you can do.  If you or any of the other voice professionals want to have fun and adlib their own sayings for free, please do 
so and I'll use them for free.

Desired format: 16-bit, Mono, 22050 Hz, Windows PCM .wavs

Marine voices
-------------
Macho sounding, confident and bad-ass, but a bit foolhardy and inexperienced.  I'm too cool
attitude.  These guys are hand-picked elite soldiers, and are working for the "cool" army.  They're not in it for the money,
they're here because they know the Frontiersmen are the best and they're the best.  I'm really trying to promote cameraderie, teamwork, and
interdependence between the marines and between the marines and their commander, so you can keep that in mind when casting.

  Need ammo - "Commander, I need ammo!", "I need ammo over here!",
  "It's gonna get ugly if I don't get another clip!", "Can anyone bring
  me some ammo?"
  Need health - "I need a medpack, stat!", "<groaning, teeth clenched>
  Mmmmh, need help here", <super-rare homage> "Frontiersman, needs
  food, badly"
  Need order - "Standin' by".  "Where are those orders?", "Ready to
  move.", "Commander, I'm standin' by."
  Order acknowledged - "I'm on it", "Consider it done"
  Objective completed - "Objective completed." 
  Taunt - "Tell me one thing...why am I sooo bad?", "How'd I get to be
  so good?", "Color me bad...-ass.", "Blood's in fashion this season",
  "I only need to know one thing...what they are", "My home!" (damn
  these are fun)
  (this is about half of them)
  (depending on costs, you could do female soldier versions of all
  these as well, and we'll use them in the future when we add a female
  model <which WON'T have big bust and skimpy amor, but will look more
  like one of the boys>)
  Enemy spotted - "<quiet, over throat mike> Enemy spotted" "We've got
  hostiles."
  Need commander - "We need a commander"
  Game starts - "Squad, let's get those 
  Misc: "All clear!", "For Ariadne!", "Now let's take back what's
  ours.", "Remember, we're not just fightin' aliens out here, we're
  fightin' for goddam law and order.", "Aliens!  Yee-freakin�-hah!",
  "Watch your fire!"

  - Commander voices (older and more mature, somewhat grizzled combat
  vet, calm and cool.  I)
  Move to waypoint - "Move to your waypoint, soldier", "Proceed into
  the structure"
  Build - "Build structure at waypoint", "
  Attack - "Attack that enemy structure", "Attack the enemy"
  Pull back - "Pull outta there...now."
  Weld target - "Weld that area area, on the double"
  (this is perhaps half of them)

  - Command station computer voice.  These are pretty standard RTS
  alerts.  This could be the standard sexy female computer voice, or
  your own take on it.  The command station that's used to guide the
  troops and build structures is actually a reprogrammed general
  maintainance computer.  It seems a little strange that it would be
  able to say "Your base is under attack", so maybe it should sound
  synthesized, or otherwise put together from a dictionary of words? 
  Only if it sounds cool though.

  - Your base is under attack
  - The command station is under attack
  - Your soldiers have engaged the enemy
  - Upgrade complete
  - Research complete

  - Alien team voices.  Should sound sound like an alien "hive" talking
  to his team.  You're perfect for this.  This could be a lot like the
  Zerg voice saying "The hive cluster is under attack".  It's bad-ass,
  animal-like, sloppy and telepathically transmitted.  Whereas the
  command station computer voice says "Your", the alien team voices say
  "our" and "we", implying the communal hive mind.  Also, the marines
  are generally male voices, and the aliens are generally female voices
  (though neither side is good nor evil, they are both species just
  trying to survive).

  - "Our hive is under attack", "Our mother, needs help", "<a little
  sad> Our hive...is dying"
  - "Our structure is under attack", "
  - "We need another hive", "We must expand to survive"
  - "We need upgrade chambers"
  - "The enemy approaches"
  - "Hive construction complete", "Our hive...is complete"
  - "You need more resources"
  - "Resources...running out"
  - "Parasite implanted"
  - "Eradicate the intruders"
  - Some grunts and pain sounds for the hive and shared organic
  structures, when they take damage.  They are all linked, and they're
  all alive.  Nothing too blood-curdling or chilling, there's no horror
  here, more of a pagan/natural/primal/animal feel.

  - Tutorial voice overs.  Depending on cost for this stuff, we could
  use some tutorial voice overs as well, though I'm not sure I can
  afford them.  These are full sentences or paragraphs, and will be
  fairly dry.  "Walk up to the structure, look at the base, and hold
  your 'use' key to build it.".  "Use your pop-up menu to ask for ammo
  and health, or to communicate with the rest of your squad.".
  
  Finally, we really need to talk about rates.  I know high-quality
  work costs good money, and I'm willing to pay it, but I do need to
  know about how much it's going to cost me either per sample, or per
  hour, so I can reduce the number of sounds we need, or add more.  

  I want you to do the alien team voices (I wouldn't even consider
  anyone else), and I'd like you to find and/or record others for the
  marine voices and commander voices.  If in my price range, I'd also
  like you to do the tutorial voice overs, though that's a lot more
  reading and probably a lot less fun.  Lastly, I've heard that you
  know some of the guys that did the macho voice overs for Unreal
  Tournament.  Maybe they would be good for this as well?

  I hope this isn't too much to take in at once, and I hope you're
  still interested in working on Natural Selection.  Let's talk on the
  phone sometime soon (today, or anytime this week), and we can discuss
  rates and other specifics.  Thanks a lot, I can't wait to get going
  on this!


