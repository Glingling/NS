Game names
  Takeaway: Space.  Meaningful experience.  Something fresh.  Drama.
  Think of what it sounds like with Half-Life: in front of it.  Half-life: Galaxy
	- Glory
	- Cleanse
	- Impure
	- Flashback
	- Delta squadron
	- Scavenger
	- The great war
	- Flash war
	- Unite
	- Host
	- Gathering
	- Reunion
	- Swarm, "The swarm"
	- Galaxy (implies both the space part and the congregation part!)
		1. Any of numerous large-scale aggregates of stars, gas, and dust that constitute the universe, containing an average of 100 billion (1011) solar masses and ranging in diameter from 1,500 to 300,000 light-years. Also called nebula. 
		Often Galaxy. The Milky Way. 
		2. An assembly of brilliant, glamorous, or distinguished persons or things: "a galaxy of theatrical performers".
	- Sentinel
	- Guardian - Nice because it works for both sides (Is "Guardians" better?)
	- Salvation (it has the accursed Half-life "-tion" suffix however)
		1. Preservation or deliverance from destruction, difficulty, or evil. 
		2. A source, means, or cause of such preservation or deliverance. 
		Theology. 
		1. Deliverance from the power or penalty of sin; redemption. 
		2. The agent or means that brings about such deliverance. 
		Christian Science. The realization and demonstration of Life, Truth, and Love as supreme over all, carrying with it the destruction of the illusions of sin, sickness, and death. 
	- Natural selection
	- Bloodlines

