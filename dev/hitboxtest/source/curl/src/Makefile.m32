#############################################################
# $Id: Makefile.m32,v 1.11 2003/02/28 15:49:33 bagder Exp $
#
## Makefile for building curl.exe with MingW32 (GCC-3.2) and
## optionally OpenSSL (0.9.6)
##
## Use: make -f Makefile.m32 [SSL=1] [DYN=1]
##
## Comments to: Troy Engel <tengel@sonic.net> or
##              Joern Hartroth <hartroth@acm.org>

CC = gcc
RM = rm -f
STRIP = strip -s
OPENSSL_PATH = ../../openssl-0.9.7a
ZLIB_PATH = ../../zlib-1.1.4

# We may need these someday
# PERL = perl
# NROFF = nroff

########################################################
## Nothing more to do below this line!

INCLUDES = -I. -I.. -I../include
CFLAGS = -g -O2 -DMINGW32
ifdef SSL
  CFLAGS += -DUSE_SSLEAY -DHAVE_OPENSSL_ENGINE_H
endif
LDFLAGS = 
COMPILE = $(CC) $(INCLUDES) $(CFLAGS)
LINK = $(CC) $(CFLAGS) $(LDFLAGS) -o $@

curl_PROGRAMS = curl.exe
curl_OBJECTS = main.o hugehelp.o urlglob.o writeout.o
curl_SOURCES = main.c hugehelp.c urlglob.c writeout.c
ifdef DYN
  curl_DEPENDENCIES = ../lib/libcurldll.a ../lib/libcurl.dll
  curl_LDADD = -L../lib -lcurldll
else
  curl_DEPENDENCIES = ../lib/libcurl.a
  curl_LDADD = -L../lib -lcurl
endif
curl_LDADD += -lwsock32 -lws2_32 -lwinmm
ifdef SSL
  curl_LDADD += -L$(OPENSSL_PATH)/out -leay32 -lssl32
endif
ifdef ZLIB
  curl_LDADD += -L$(ZLIB_PATH) -lz
endif

PROGRAMS = $(curl_PROGRAMS)
SOURCES = $(curl_SOURCES)
OBJECTS = $(curl_OBJECTS)

all: curl.exe

curl.exe: $(curl_OBJECTS) $(curl_DEPENDENCIES)
	$(RM) $@
	$(LINK) $(curl_OBJECTS) $(curl_LDADD)
	$(STRIP) $@

# We don't have nroff normally under win32
# hugehelp.c: ../README.curl ../curl.1 mkhelp.pl
# 	$(RM) hugehelp.c
# 	$(NROFF) -man ../curl.1 | $(PERL) mkhelp.pl ../README.curl > hugehelp.c

.c.o:
	$(COMPILE) -c $<

.s.o:
	$(COMPILE) -c $<

.S.o:
	$(COMPILE) -c $<

clean:
	$(RM) $(curl_OBJECTS) curl.exe

distrib: clean
	$(RM) $(curl_PROGRAMS)
